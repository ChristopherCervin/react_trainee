import React from 'react';
import './App.css';
import {Link} from 'react-router-dom'; 

interface JProps {
  id: number,
  name: string,
  img: string,
  description: string,
}


const QuizCard: React.FC<JProps> = ({id, name, img, description}) => {
  return (
    <Link to={"/quiz_"+id}>
    <div className="quiz-card">
      <h2>{ name }</h2>
      <img src={ img } />
      <p>{ description }</p>
      
    </div>    
    </Link>

  );
}


export default QuizCard;
