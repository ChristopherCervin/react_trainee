import React from 'react';
import './App.css';
import QuizCard from './QuizCard';


interface IProps {
    quiz: Array<{
        id: number,
        name: string,
        img: string,
        description: string
    }>,
  }

const QuizList: React.FC<IProps> = ({quiz}) => {
    return (
        <ul>
            { quiz.map(q => (
                <QuizCard {...q} />
                
            ))}
        </ul>
    );
}

export default QuizList;
