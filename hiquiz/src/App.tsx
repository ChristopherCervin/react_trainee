import React from 'react';
import './App.css';
import QuizList from './QuizList';
import Quiz from './Quiz'
import {Route, Switch} from 'react-router-dom';

// export interface QuizElements {
//   name: string,
//   img: string,
//   description: string,
// }

const QuizElements = [
  { id:1, name: "Star Wars", img: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Star_Wars_Logo.svg/330px-Star_Wars_Logo.svg.png", questions: ["Q1", "Q2"], description: "Let's check what you know about Star Wars!"},
  { id:2, name: "Dogs", img: "https://www.petmd.com/sites/default/files/Acute-Dog-Diarrhea-47066074.jpg",questions: ["Q1", "Q2"], description: "Let's check what you know about dogs!"},
  { id:3, name: "80's TV Shows", img: "https://bfmbrainfall.files.wordpress.com/2016/04/can_you_identify_these_80s_tv_shows_featured_large.jpg?w=468",questions: ["Q1", "Q2"], description: "Let's check what you know about 80's TV Shows!"}
]

class App extends React.Component {
  render(){
    return (
      <div>
        <h1>HiQuiz</h1>
        <Switch>
        <Route exact path="/" render = {() => <QuizList quiz={QuizElements}/>}/>
        
        {QuizElements.map( element => {
          return <Route path={"/quiz_"+element.id} render = {() => <Quiz/>}/>
        })}

        </Switch>
      </div>
    );
  }
}



export default App;
